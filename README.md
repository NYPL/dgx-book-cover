# Book Cover Component

This component is used to display a book cover item.

## Version
> v0.1.3

## Usage

> Require `dgx-book-cover` as a dependency in your `package.json` file.

```sh
"dgx-book-cover": "git+ssh://git@bitbucket.org/NYPL/dgx-book-cover.git#master"
```

> Once installed, import the component in your React Application.

```sh
import BookCover from 'dgx-book-cover';
```

## Props

> You may initialize the component with the following properties:

```sh
<BookCover
  id="bookCover" ## String (default: 'BookCover')
  className="bookCover" ## String (optional)
  title="Book Title" ## String representation of the book title
  imgAlt="Alt Text" ## String representation of the alternate image text
  target="https://..." ## String representation of the catalog URL for the specified book
  imgSrc="https://..." ## String representation of the book cover image
  author="Book Author" ## String representation of the book author
  genre="Book Genre" ## String representation of the book genre
  audience="Book Audience" ## String representation of the book audience
  gaClickEvent={gaClickFunc()} ## Function for Google Analytics click events. The Action is determined by the gaActionText property and concatenated with the item number. The Label is the URL for each feature element (Optional)
  gaActionText="Of Note" ## String representing the base of what the Action field will be on each feature item (Optional)
  bookId={index} ## Integer representing the index of the BookCover item. This field is used to generate the proper Google Analytics action text when this component is used in a loop. (Optional)
/>
```

## Local Development Setup
Run `npm install` to install all dependencies from your package.json file.

Next, startup the Webpack Development Server by running `npm start`. Visit `localhost:3000` in your web browser and begin coding.

> **NOTE:** We are currently using Webpack Hot Reload Server configuration which allows you to change your code without restarting your browser.

Once you have completed any code updates, ensure to run `npm run babel-build` in your terminal. This will build/bundle all your code in the `dist/` path via Webpack.
