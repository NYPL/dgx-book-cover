import React from 'react';
import PropTypes from 'prop-types';

class BookCover extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      imageSrc: this.props.imgSrc,
      // The original width of the source image
      naturalWidth: 150,
      errorStatus: 'one-pixel',
    };

    this.handleLoadedImage = this.handleLoadedImage.bind(this);
    this.handleLoadedImageError = this.handleLoadedImageError.bind(this);
  }

  shouldComponentUpdate(nextProps) {
    if (this.state.title !== nextProps.title &&
      this.state.imageSrc !== nextProps.imgSrc) {
      this.setState({
        imageSrc: nextProps.imgSrc,
        errorStatus: '',
      });
      return true;
    }

    if (!nextProps.imgSrc || this.state.title !== nextProps.title) {
      return true;
    }

    return false;
  }

  /**
   * getGaActionText()
   * Returns the string representation of the desired Google Analytics Action. Verifies if various
   * properties are defined to concatenate into a final string. gaActionText and bookIndex
   * properties are optional, if no title property is defined, null will be returned.
   */
  getGaActionText() {
    let actionText = '';
    if (!this.props.title) {
      return null;
    }

    actionText = this.props.title;

    if (this.props.gaActionText) {
      actionText = `${this.props.gaActionText} - ${actionText}`;
    }

    if (this.props.bookIndex >= 0) {
      actionText = `${actionText} - ${this.props.bookIndex + 1}`;
    }

    return actionText;
  }

  handleLoadedImage() {
    const width = this.refs.coverImage.naturalWidth;

    if (width < 10 && width >= 0) {
      this.setState({
        errorStatus: 'one-pixel',
      });
    } else {
      this.setState({
        errorStatus: '',
      });
    }

    this.forceUpdate();
  }

  handleLoadedImageError() {
    this.setState({
      errorStatus: 'one-pixel',
      imageSrc: '',
    });
    this.forceUpdate();
  }

  render() {
    return (
      <a
        href={this.props.target}
        className={`${this.props.className} book-cover`}
        id={this.props.id}
        onClick={this.props.gaClickEvent ?
          () => this.props.gaClickEvent(this.getGaActionText(), this.props.target) : null
        }
      >
        <img
          onLoad={this.handleLoadedImage}
          onError={this.handleLoadedImageError}
          id={`cover-${this.props.id}`}
          className={this.state.errorStatus}
          ref="coverImage"
          src={this.state.imageSrc}
          title={this.props.title}
          alt={this.props.imgAlt}
        />
        <div className="itemOverlay">
          <p className="title">{this.props.title}</p>
          <div className="details">
            <p className="details-author">{this.props.author}</p>
            <p className="details-audience">{this.props.audience}</p>
            <p className="details-genre">{this.props.genre}</p>
          </div>
        </div>
      </a>
    );
  }
}

BookCover.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  title: PropTypes.string,
  imgAlt: PropTypes.string,
  target: PropTypes.string,
  imgSrc: PropTypes.string,
  author: PropTypes.string,
  genre: PropTypes.string,
  audience: PropTypes.string,
  gaClickEvent: PropTypes.func,
  gaActionText: PropTypes.string,
  bookIndex: PropTypes.number,
};

BookCover.defaultProps = {
  id: 'BookCover',
  genre: 'Fiction',
  author: '',
  audience: 'Adult',
};

export default BookCover;
