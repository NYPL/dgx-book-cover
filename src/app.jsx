import React from 'react';
import { render } from 'react-dom';
import BookCover from './components/BookCover.jsx';

import './styles/main.scss';

const books = [
  {
    image: {
      bookCoverImage: {
        'full-uri': 'https://d7.nypl.org/sites/default/files/Chocky.jpg',
        description: 'This is the description for the book cover',
        alt: 'This is the alt for the book cover',
      },
    },
    link: 'http://browse.nypl.org/iii/encore/record/C__Rb20720345__SChocky%20__Orightresult' +
      '__U__X7?lang=eng&suite=def',
    title: { en: { text: 'Chocky' } },
    author: {
      fullName: 'John Wyndham',
    },
    bookItem: {
      audience: 'Adult',
      genre: 'Creepy',
    },
  },
  {
    image: {
      bookCoverImage: {
        'full-uri': 'https://d7.nypl.org/sites/default/files/Dietland.jpg',
        description: 'This is the description for the book cover',
        alt: 'This is the alt for the book cover',
      },
    },
    link: 'http://browse.nypl.org/iii/encore/record/C__Rb20617276__SDietland__Orightresult' +
      '__U__X7?lang=eng&suite=def',
    title: { en: { text: 'Dietland' } },
    author: {
      fullName: 'Sarai Walker',
    },
    bookItem: {
      audience: 'Young Adult',
      genre: 'Edgy',
    },
  },
  {
    image: {
      bookCoverImage: {
        'full-uri': 'https://d7.nypl.org/sites/default/files/Fifty_Mice.jpg',
        description: 'This is the description for the book cover',
        alt: 'This is the alt for the book cover',
      },
    },
    link: 'http://browse.nypl.org/iii/encore/record/C__Rb20333845__SFifty%20Mice__Oright' +
      'result__U__X7?lang=eng&suite=def',
    title: { en: { text: 'Fifty_Mice' } },
    author: {
      fullName: 'Daniel Pyne',
    },
    bookItem: {
      audience: 'Young Adult',
      genre: 'Otherworldy',
    },
  },
  {
    image: {
      bookCoverImage: {
        'full-uri': 'https://d7.nypl.org/sites/default/files/Furiously_Happy.jpg',
        description: 'This is the description for the book cover',
        alt: 'This is the alt for the book cover',
      },
    },
    link: 'http://browse.nypl.org/iii/encore/record/C__Rb20814005__SFuriously%20Happy%3A%' +
      '20a%20Funny%20Book%20About%20Horrible%20Things__Orightresult__U__X7?lang=eng&suite=def',
    title: { en: { text: 'Furiously_Happy' } },
    author: {
      fullName: 'Jenny Lawson',
    },
    bookItem: {
      audience: 'Children',
      genre: 'Funny',
    },
  },
  {
    image: {
      bookCoverImage: {
        'full-uri': 'https://d7.nypl.org/sites/default/files/Incognito.jpg',
        description: 'This is the description for the book cover',
        alt: 'This is the alt for the book cover',
      },
    },
    link: 'http://browse.nypl.org/iii/encore/record/C__Rb18403039__SIncognito__P0%2C1__' +
      'Orightresult__U__X7?lang=eng&suite=def',
    title: { en: { text: 'Incognito -- fake long title' } },
    author: {
      fullName: 'Ed Brubaker',
    },
    bookItem: {
      audience: 'Adult',
      genre: 'Historical',
    },
  },
];

// Used to mock gaClick event
const gaClickTest = () => (
  (action, label) => {
    console.log(action);
    console.log(label);
  }
);

const booklist = books.map((book, i) => (
  <li key={i} style={{ display: 'inline-block', marginRight: '10px' }}>
    <BookCover
      id="first-cover"
      title={book.title.en.text}
      target={book.link}
      author={book.author.fullName}
      imgAlt={book.image.bookCoverImage.alt}
      imgSrc={book.image.bookCoverImage['full-uri']}
      tag={book.tag}
      gaClickEvent={gaClickTest()}
      gaActionText="New & Noteworthy"
      bookIndex={i}
    />
  </li>
));

render(<ul>{booklist}</ul>, document.getElementById('books'));
