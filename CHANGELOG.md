## Changelog

### v0.2.0
- Updated to React 15.
- Updated webpack process to build.

### v0.1.3
#### Added
- Added support for Google Analytics click events via `gaClickEvent` function property.
- Added `gaActionText` & `bookItem` property to generate a proper Google Analytics action text.

#### Changed
- Updated README.md file to contain a CHANGELOG and enhanced component description.
